import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceService } from 'src/app/Service/service.service';
import { Usuario } from 'src/app/Modelo/Usuario';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {
  user: Usuario = new Usuario();

  constructor(private router: Router, private service: ServiceService) {
  }

  ngOnInit(): void {
  }

  Guardar(nombre: string, email: string, password: string) {
    this.user.name = nombre;
    this.user.email = email;
    this.user.password = password;
    this.service.createUsuario(this.user)
      .subscribe(data => {
        alert('Usuario agregado!!');
        this.router.navigate(['listar']);
      })
  }
}
