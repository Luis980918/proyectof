import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EdittserviciosComponent } from './edittservicios.component';

describe('EditComponent', () => {
  let component: EdittserviciosComponent;
  let fixture: ComponentFixture<EdittserviciosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EdittserviciosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EdittserviciosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
