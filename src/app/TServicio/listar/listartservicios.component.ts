import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceTservicio } from '../../Service/service.tservicio';
import { TServicio } from 'src/app/Modelo/TServicio';

@Component({
  selector: 'app-listar',
  templateUrl: './listartservicios.component.html',
  styleUrls: ['./listartservicios.component.css']
})
export class ListartserviciosComponent implements OnInit {

  servicios: TServicio[];
  constructor(private service: ServiceTservicio, private router: Router) { }

  ngOnInit(){
    this.service.getTServicio()
    .subscribe(data => {
      this.servicios = data;
      // tslint:disable-next-line:semicolon
    })
  }
}
