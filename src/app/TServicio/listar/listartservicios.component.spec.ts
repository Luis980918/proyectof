import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListartserviciosComponent } from './listartservicios.component';

describe('listarTservicio', () => {
  let component: ListartserviciosComponent;
  let fixture: ComponentFixture<ListartserviciosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListartserviciosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListartserviciosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
