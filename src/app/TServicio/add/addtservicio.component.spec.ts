import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddtservicioComponent } from './addtservicio.component';

describe('AddComponent', () => {
  let component: AddtservicioComponent;
  let fixture: ComponentFixture<AddtservicioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddtservicioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddtservicioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
