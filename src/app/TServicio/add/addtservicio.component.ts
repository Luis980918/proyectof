import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceTservicio } from 'src/app/Service/service.tservicio';
import {TServicio} from '../../Modelo/TServicio';


@Component({
  selector: 'app-add',
  templateUrl: './addtservicio.component.html',
  styleUrls: ['./addtservicio.component.css']
})
export class AddtservicioComponent implements OnInit {
  tservicio: TServicio = new TServicio();
  constructor( private router: Router, private service: ServiceTservicio) { }

  ngOnInit(): void {
  }
  Guardar(nameTServicio: string, costo: number, servicioRelacionado: string){
    this.tservicio.nombreServicio = nameTServicio;
    this.tservicio.costoServicio = costo;
    this.tservicio.servicio = servicioRelacionado;
    this.service.CreateTServicios(this.tservicio)
      .subscribe(data => {alert('Tipo de Servicio agregado!!'); this.router.navigate(['listarTservicio']);});
  }
}
