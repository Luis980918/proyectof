import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListarComponent } from './Usuario/listar/listar.component';
import { AddComponent } from './Usuario/add/add.component';
import { EditComponent } from './Usuario/edit/edit.component';
import {FormsModule} from '@angular/forms';
import { ServiceService } from '../app/Service/service.service';
import {HttpClientModule} from '@angular/common/http';

import {AddPrestadorComponent} from './Prestador/add/addPrestador.component';
import {EditPrestadorComponent} from './Prestador/edit/editPrestador.component';
import {ListarPrestadorComponent} from './Prestador/listar/listarPrestador.component';

import {AddtservicioComponent} from './TServicio/add/addtservicio.component';
import {EdittserviciosComponent} from './TServicio/edit/edittservicios.component';
import {ListartserviciosComponent} from './TServicio/listar/listartservicios.component';


// @ts-ignore
@NgModule({
  declarations: [
    AddComponent,
    AppComponent,
    ListarComponent,
    AddComponent,
    EditComponent,

    AddPrestadorComponent,
    EditPrestadorComponent,
    ListarPrestadorComponent,

    AddtservicioComponent,
    EdittserviciosComponent,
    ListartserviciosComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
  ],
  providers: [ServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
