import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TServicio } from '../Modelo/TServicio';
@Injectable({
  providedIn: 'root'
})
export class ServiceTservicio {
  constructor(private http: HttpClient) { }
  Url = 'http://localhost:8080/v1/tservicio';
  getTServicio(){
    return this.http.get<TServicio[]>(this.Url);
  }
  CreateTServicios(tServicios: TServicio){
    return this.http.post<TServicio>(this.Url, tServicios);
  }
}
