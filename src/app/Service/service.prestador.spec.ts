import { TestBed } from '@angular/core/testing';

import { ServicePrestador } from './service.prestador';

describe('ServiceService', () => {
  let service: ServicePrestador;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ServicePrestador);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
