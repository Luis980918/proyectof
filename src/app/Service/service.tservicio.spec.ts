import { TestBed } from '@angular/core/testing';

import { ServiceTservicio } from './service.tservicio';

describe('ServiceTservicio', () => {
  let service: ServiceTservicio;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ServiceTservicio);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
