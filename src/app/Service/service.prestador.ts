import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Prestador } from '../Modelo/Prestador';
@Injectable({
  providedIn: 'root'
})
export class ServicePrestador {
  constructor(private http: HttpClient) { }
  Url = 'http://localhost:8080/api/v1/prestadores';
  getPrestadores(){
    return this.http.get<Prestador[]>(this.Url);
  }
}
