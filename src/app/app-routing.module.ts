import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListarComponent } from './Usuario/listar/listar.component';
import { AddComponent } from './Usuario/add/add.component';
import { EditComponent } from './Usuario/edit/edit.component';
import { ListartserviciosComponent } from './TServicio/listar/listartservicios.component';
import { AddtservicioComponent } from './TServicio/add/addtservicio.component';
import { ListarPrestadorComponent } from './Prestador/listar/listarPrestador.component';
import { AddPrestadorComponent } from './Prestador/add/addPrestador.component';

const routes: Routes = [
  {path: 'listar', component: ListarComponent},
  {path: 'add', component: AddComponent},
  {path: 'edit', component: EditComponent},
  {path: 'listartservicios', component: ListartserviciosComponent},
  {path: 'addtservicios', component: AddtservicioComponent},
  {path: 'listarprestador', component: ListarPrestadorComponent},
  {path: 'addprestador', component: AddPrestadorComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
