import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ProyectoPA';

  constructor(private router: Router){}

  Listar(){
    this.router.navigate(['listar']);
  }
  Listarp(){
    this.router.navigate(['listarprestador']);
  }
  ListarTs(){
    this.router.navigate(['listartservicios']);
  }
  Nuevo(){

    this.router.navigate([ 'add']);
  }
  Nuevots(){
    this.router.navigate([ 'addtservicios']);
  }
}
