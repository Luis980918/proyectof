import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditPrestadorComponent } from './editPrestador.component';

describe('EditComponent', () => {
  let component: EditPrestadorComponent;
  let fixture: ComponentFixture<EditPrestadorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditPrestadorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditPrestadorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
