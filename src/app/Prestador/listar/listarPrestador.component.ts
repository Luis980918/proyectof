import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServicePrestador } from '../../Service/service.prestador';
import { Prestador } from 'src/app/Modelo/Prestador';

@Component({
  selector: 'app-listar',
  templateUrl: './listarPrestador.component.html',
  styleUrls: ['./listarPrestador.component.css']
})
export class ListarPrestadorComponent implements OnInit {

  prestadores: Prestador[];
  constructor(private service: ServicePrestador, private router: Router) { }

  ngOnInit(){
    this.service.getPrestadores()
    .subscribe(data => {
      this.prestadores = data;
      // tslint:disable-next-line:semicolon
    })
  }
}
