import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListarPrestadorComponent } from './listarPrestador.component';

describe('ListarComponent', () => {
  let component: ListarPrestadorComponent;
  let fixture: ComponentFixture<ListarPrestadorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListarPrestadorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarPrestadorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
