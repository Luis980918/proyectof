import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddPrestadorComponent } from './addPrestador.component';

describe('AddComponent', () => {
  let component: AddPrestadorComponent;
  let fixture: ComponentFixture<AddPrestadorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddPrestadorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddPrestadorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
